Account Time Contingent
=======================

Some words about why this module was created...

.. contents:: Table of Content
   :depth: 2

The following chapters will give you some inputs about configuration and usage
of the features:

Feature 1
---------

Usage
^^^^^

How to use it

Configuration Options
^^^^^^^^^^^^^^^^^^^^^

How to configure it

For a screenshot with parameters explained add a

.. figure:: screenshot-example.png
   :figwidth: 800px
   :align: center
   :height: 600px 
   :width: 800px
   :scale: 100 %
   :alt: alternate text

   Screenshot caption

   +-----------------------+-----------------------+
   | Parameter / Symbol    | Meaning               |
   +=======================+=======================+
   | Parameter name        | Parameter description |
   +-----------------------+-----------------------+
   | .. image:: button.png | Description of button |
   |    :height: 64px      |                       |
   |    :width:  64px      |                       |
   +-----------------------+-----------------------+


Sub Feature 1.1
^^^^^^^^^^^^^^^

More information about sub feature...

Subtitle for sub feature
~~~~~~~~~~~~~~~~~~~~~~~~

Maybe you need a python code block?

.. code-block:: python

    mydict = {
        'name': 'value',
    }


adding some external links with link-name_ defined at the end of the document...


Todo's
------

- List of planned, but not yet implemented features
  Maybe there are also some missing error handling routines.
- List of missing features, which are not planned but possible

Here is the definition of external links, which are invisible

.. _link-name: https://jamotion.ch
