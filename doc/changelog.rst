Changelog
=========

Version 8.0.1.0.0
-----------------
Feature 1463: Initial release
    - New Tab "Time contingents" on contracts with table
      "analytic.time.contingent" showing fields "Analytic Account",
      "Purchase Date", "Time Amount" and "Invoice".
    - Time in "Units consumed" now only takes in account non invoiceable time
      when calculated.
    - New field "units consumed but invoiced", showing time invoiced
      separately.
    - Emails sent automatically, when time contingent drops under 10% and when
      contingent expires.

Follow-ups:
    - Warning messages when analytic account with low or expired time
      contingents is selected on timesheet.
    - Set cron job to run daily.
    - Clean up module folder structure.
    - Code refactoring.