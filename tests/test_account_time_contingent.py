# -*- coding: utf-8 -*-
##############################################################################
#
# OpenERP, Open Source Business Applications
# Copyright (c) 2012-TODAY OpenERP S.A. <http://openerp.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging

from openerp import fields

import openerp
import openerp.tests

from openerp.tests import common
from openerp.exceptions import ValidationError

_logger = logging.getLogger(__name__)
_testall = True

# Todo: write unittests for module
@openerp.tests.common.at_install(_testall)
@openerp.tests.common.post_install(False)
class TestSample(common.TransactionCase):
    def setUp(self):
        super(TestSample, self).setUp()

    def test_001_sample_one(self):
        self.assertEqual('1', '1')
