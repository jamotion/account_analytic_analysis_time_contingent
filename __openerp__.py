# -*- coding: utf-8 -*-
{
    'name': 'Contract Time Contingent',
    'version': '8.0.1.0.0',
    'category': 'Category',
    'author':  'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Enhanced calculation of time contingents and notification '
               'on maintenance contracts.',
    'images': [],
    'depends': [
        'analytic',
        'sale',
        'account_analytic_analysis',
    ],
    'data': [
        'data/data_analysis_time_contingent.xml',
        'views/account_analytic_analysis_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'test': [],
    'application': False,
    'active': False,
    'installable': True,
}

