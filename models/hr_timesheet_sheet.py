# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 17.08.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class HrAnalyticTimesheet(models.Model):
    # Private attributes
    _inherit = 'hr.analytic.timesheet'

    # Default methods

    # Fields declaration

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
    @api.multi
    def on_change_account_id(self, account_id, user_id=False):
        result = super(HrAnalyticTimesheet, self)\
            .on_change_account_id(account_id=account_id, user_id=user_id)

        contract = self.env['account.analytic.account'].browse(account_id)

        if contract.remaining_hours <= 0 and contract.use_time_contingents:
            result['warning'] = {
                'title': _(u'Warning'),
                'message': _(u'The time contingents on this analytic account '
                             u'are expired! Either set up a new contingent or '
                             u'set the record to invoiceable.')
            }
        elif contract.is_contingent_low and contract.use_time_contingents:
            result['warning'] = {
                'title': _(u'Warning'),
                'message': _(u'There is less than 10% of the original time '
                             u'amount left on this analytic account! The '
                             u'remaining Time amount is {0} hours').format(
                    contract.remaining_hours),
            }
        return result
