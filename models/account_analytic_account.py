# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebastien.pulver on 05.12.2016.
#

# imports of python lib
import logging
import math

# imports of openerp
import datetime
import time

# imports from odoo modules
from openerp import models, fields, api, _

# global variable definitions
_logger = logging.getLogger(__name__)


class AccountAnalyticAccount(models.Model):
    # Private attributes
    _inherit = 'account.analytic.account'

    # Fields declaration
    hours_invoiced_quantity = fields.Float(
        string='Invoiceable Time',
        compute='_compute_hours_quantity',
    )

    hours_quantity = fields.Float(
        string='Consumed Time of Contingent',
        compute='_compute_hours_quantity',
    )

    is_contingent_low = fields.Boolean(
        default=False,
        compute='_compute_is_contingent_low',
    )

    quantity_max = fields.Float(
        string='Prepaid Service Units',
        compute='_compute_quantity_max',
        help='Sets the higher limit of time to work on the contract, '
             'based on the timesheet. (for instance, number of hours in '
             'a limited support contract.)',
        readonly=True,
        store=True,
    )

    time_contingent_ids = fields.One2many(
        comodel_name='analytic.time.contingent',
        inverse_name='account_id',
    )

    use_time_contingents = fields.Boolean(
        string="Use Time Contingents",
    )

    # Compute and search methods, in the same order as fields declaration
    @api.multi
    def _compute_hours_quantity(self):
        analytic_line_obj = self.env['account.analytic.line']
        journal_id = self.env.ref('hr_timesheet.analytic_journal')
        all_lines = analytic_line_obj.search([
            ('account_id', 'in', self.ids),
            ('journal_id', '=', journal_id.id)])
        for record in self:
            lines = all_lines.filtered(lambda f: f.account_id == record)
            if not lines:
                continue
            invoiced_lines = lines.filtered(lambda f: f.to_invoice)
            contingent_lines = lines.filtered(lambda f: not f.to_invoice)
            record.hours_invoiced_quantity = \
                sum(invoiced_lines.mapped('unit_amount'))
            record.hours_quantity = \
                sum(contingent_lines.mapped('unit_amount'))

    @api.multi
    def _compute_is_contingent_low(self):
        for record in self:
            latest_contingent = \
                record.time_contingent_ids.sorted(
                    key=lambda k: k.purchase_date)
            if not latest_contingent:
                continue
            # Remaining time is less than 10% of last purchased contingent,
            # rounded to half hours
            latest_contingent = latest_contingent[-1]
            contingent_limit = math.ceil(
                latest_contingent.time_amount * 0.1 * 2) / 2
            if contingent_limit < 1:
                contingent_limit = 1

            record.is_contingent_low = \
                record.remaining_hours < contingent_limit

    @api.multi
    @api.depends('time_contingent_ids.time_amount')
    def _compute_quantity_max(self):
        for record in self:
            record.quantity_max = sum(
                record.time_contingent_ids.mapped('time_amount'))

    # CRUD methods

    # Mail reminder:
    def cron_account_analytic_account(self, cr, uid, context=None):
        context = dict(context or {})
        remind = {}
        print 'Jamo: cronjob started'

        def fill_remind(key, domain, write_pending=False):
            base_domain = [
                ('type', '=', 'contract'),
                ('partner_id', '!=', False),
                ('manager_id', '!=', False),
                ('manager_id.email', '!=', False),
            ]
            base_domain.extend(domain)

            accounts_ids = self.search(
                cr, uid, base_domain, context=context, order='name asc')
            accounts = self.browse(cr, uid, accounts_ids, context=context)
            for account in accounts:
                if write_pending:
                    account.write({'state': 'pending'})
                remind_user = remind.setdefault(account.manager_id.id, {})
                remind_type = remind_user.setdefault(key, {})
                remind_partner = remind_type.setdefault(
                    account.partner_id, []).append(account)

        # Already expired
        fill_remind("old", [('state', 'in', ['pending'])])

        # Expires now
        fill_remind("new", [('state', 'in', ['draft', 'open']), '|', '&',
                            ('date', '!=', False),
                            ('date', '<=', time.strftime('%Y-%m-%d')),
                            ('is_overdue_quantity', '=', True)], True)

        # Expires in less than 30 days
        fill_remind("future", [('state', 'in', ['draft', 'open']),
                               ('date', '!=', False),
                               ('date', '<', (datetime.datetime.now()
                                              + datetime.timedelta(30))
                                .strftime('%Y-%m-%d'))])

        # Less than 10 percent of time contingent left
        fill_remind("contingent_low", [('state', 'in', ['draft', 'open']),
                                       ('is_contingent_low', '=', True)
                                       ], False)

        context['base_url'] = self.pool.get(
            'ir.config_parameter').get_param(cr, uid, 'web.base.url')
        context['action_id'] = \
            self.pool.get('ir.model.data').get_object_reference(
                cr, uid, 'account_analytic_analysis',
                'action_account_analytic_overdue_all')[1]
        template_id = \
            self.pool.get('ir.model.data').get_object_reference(
                cr, uid, 'account_analytic_analysis',
                'account_analytic_cron_email_template')[1]
        for user_id, data in remind.items():
            context['data'] = data
            _logger.debug('Sending reminder to uid %s', user_id)
            self.pool.get('email.template') \
                .send_mail(cr, uid, template_id, user_id,
                           force_send=True, context=context)
        return True


class AnalyticTimeContingent(models.Model):
    _name = 'analytic.time.contingent'
    _description = 'Time Contingent on Contract'
    _order = 'account_id'

    account_id = fields.Many2one(
        comodel_name='account.analytic.account',
        string="Analytic Account",
        ondelete="cascade",
    )

    invoice_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Invoice',
        ondelete='cascade',
        required=True,
    )

    purchase_date = fields.Date(
        string='Purchase Date',
        required=True,
    )

    time_amount = fields.Float(
        string='Time Amount',
        required=True,
    )
